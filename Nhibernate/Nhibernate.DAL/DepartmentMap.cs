﻿using FluentNHibernate.Mapping;

namespace Nhibernate.DAL
{
    class DepartmentMap : ClassMap<Department>
    {
        //Constructor
        public DepartmentMap()
        {
            Id(x => x.Id);
            Map(x => x.Name);
            Map(x => x.PhoneNumber);
            Table("Department");
        }
    }
}

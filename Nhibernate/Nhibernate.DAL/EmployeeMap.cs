﻿using FluentNHibernate.Mapping;

namespace Nhibernate.DAL
{
    class EmployeeMap : ClassMap<Employee>
    {
        //Constructor
        public EmployeeMap()
        {
            Id(x => x.Id);
            Map(x => x.FirstName);
            Map(x => x.Position);
            References(x => x.EmployeeDepartment).Column("DepartmentId");
            Table("Employee");
        }
    }
}
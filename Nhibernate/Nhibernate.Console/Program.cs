﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nhibernate.DAL;

namespace Nhibernate.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var session = NHibernateHelper.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var DepartmentObject = new Department { Name = "IT", PhoneNumber = "962788700227" };
                    session.Save(DepartmentObject);
                    transaction.Commit();
                    System.Console.WriteLine("Department Created: " + DepartmentObject.Name);
                }
            }
        }
    }
}
